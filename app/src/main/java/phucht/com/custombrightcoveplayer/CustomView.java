package phucht.com.custombrightcoveplayer;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.VideoListener;
import com.brightcove.player.event.AbstractComponent;
import com.brightcove.player.event.Component;
import com.brightcove.player.event.Default;
import com.brightcove.player.event.Event;
import com.brightcove.player.event.EventEmitter;
import com.brightcove.player.event.EventListener;
import com.brightcove.player.event.RegisteringEventEmitter;
import com.brightcove.player.mediacontroller.BrightcoveMediaController;
import com.brightcove.player.model.DeliveryType;
import com.brightcove.player.model.Video;
import com.brightcove.player.util.ErrorUtil;
import com.brightcove.player.util.StringUtil;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by oldmen on 3/1/18.
 */

public class CustomView extends RelativeLayout implements Component {
    private final String TAG = this.getClass().getName();
    private int endTime = 0;
    private Boolean isShowing = false;
    private View mControlBar;
    private final AccessibilityManager mAccessibilityManager;
    private BrightcoveExoPlayerVideoView mPlayer;
    private Context mContext;
    private static final int sDefaultTimeout = 3000;
    private TextView mCurrTime;
    private TextView mEndTime;
    private Button mPlayBtn;
    private ProgressBar mProgressBar;
    private SeekBar mSeekBar;
    private SeekBar mVolumeSeekBar;

    // AbstractComponent

    protected EventEmitter eventEmitter;
    protected Map<String, Integer> listenerTokens;

    public void addListener(String eventType, EventListener listener) {
        this.listenerTokens.put(eventType, this.eventEmitter.on(eventType, listener));
    }

    public void addOnceListener(String eventType, EventListener listener) {
        this.listenerTokens.put(eventType, this.eventEmitter.once(eventType, listener));
    }

    public void removeListener(String eventType) {
        if(this.listenerTokens.containsKey(eventType)) {
            this.eventEmitter.off(eventType, (Integer) this.listenerTokens.get(eventType));
        }

    }

    public void removeListeners() {
        Iterator var1 = this.listenerTokens.keySet().iterator();

        while(var1.hasNext()) {
            String key = (String)var1.next();
            this.eventEmitter.off(key, (Integer) this.listenerTokens.get(key));
        }

        this.listenerTokens.clear();
    }

    public EventEmitter getEventEmitter() {
        return this.eventEmitter;
    }

    //

    private void initAbstractComponent() {
        this.eventEmitter = mPlayer.getEventEmitter();
        this.listenerTokens = new HashMap();
        if(eventEmitter == null) {
            throw new IllegalArgumentException(ErrorUtil.getMessage("eventEmitterRequired"));
        } else {

        }
    }

    public CustomView(Context context) {
        super(context);
        mContext = context;
        mAccessibilityManager = (AccessibilityManager) mContext.getSystemService(Context.ACCESSIBILITY_SERVICE);

        initLayout();
        initAbstractComponent();
        initPlayer();
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mAccessibilityManager = (AccessibilityManager) mContext.getSystemService(Context.ACCESSIBILITY_SERVICE);

        initLayout();
        initAbstractComponent();
        initPlayer();
    }

    private void initLayout() {
        inflate(mContext, R.layout.custom_brightcove, this);
        mPlayer = findViewById(R.id.player);
        mControlBar = findViewById(R.id.brightcove_control);
        mCurrTime = findViewById(R.id.current_time);
        mEndTime = findViewById(R.id.end_time);
        mPlayBtn = findViewById(R.id.play);
        mProgressBar = findViewById(R.id.progress_bar);
        mSeekBar = findViewById(R.id.custom_seek_bar);
        mVolumeSeekBar = findViewById(R.id.volume_seek);

        mProgressBar.setMax(100);
        mProgressBar.setProgress(0);
        mProgressBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.custom_progress));

        mSeekBar.setMax(100);
        mSeekBar.setProgress(0);
        mSeekBar.getThumb().mutate().setAlpha(0);
        mSeekBar.setPadding(0, 0, 0, 0);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mProgressBar.setProgress(progress);

                float msec = (progress / 100.0f) * endTime;
                mPlayer.seekTo((int) msec);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        mPlayBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                    mPlayBtn.setBackground(mContext.getResources().getDrawable(R.drawable.play));
                }
                else {
                    mPlayer.start();
                    mPlayBtn.setBackground(mContext.getResources().getDrawable(R.drawable.pause));
                }
            }
        });

        final AudioManager leftAm = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = leftAm.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int curVolume = leftAm.getStreamVolume(AudioManager.STREAM_MUSIC);
        mVolumeSeekBar.setMax(maxVolume);
        mVolumeSeekBar.setProgress(curVolume);
        mVolumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                leftAm.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        mPlayer.setMediaController((BrightcoveMediaController) null);

        Video video = Video.createVideo("http://media.w3.org/2010/05/sintel/trailer.mp4", DeliveryType.MP4);
        mPlayer.add(video);
        mPlayer.getAnalytics().setAccount("1760897681001");
        mPlayer.start();
        mPlayBtn.setBackground(mContext.getResources().getDrawable(R.drawable.pause));
    }

    private void initPlayer() {

        initEventListener();
    }

    private void initEventListener() {
        this.addListener("videoDurationChanged", new VideoDurationChangedHandler());
        this.addListener("progress", new ProgressHandler());
        this.addListener("seekTo", new SeekToHandler());
    }

    private void initButtons() {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        doubleTapDetector.onTouchEvent(event);
        Log.d("MController", "onTouchEvent");
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                show();
                break;
            case MotionEvent.ACTION_UP:
                show();
                break;
            case MotionEvent.ACTION_CANCEL:
                hide();
                break;
            default:
                break;
        }
        return false;
    }

    public void show() { show(sDefaultTimeout); }

    public void show(int timeout) {
        if (!isShowing) {
            mControlBar.setVisibility(View.VISIBLE);
        }
        isShowing = true;

        if (timeout != 0 && !mAccessibilityManager.isTouchExplorationEnabled()) {
            removeCallbacks(mFadeOut);
            postDelayed(mFadeOut, timeout);
        }
    }

    public void hide() {
        if (isShowing) {
            mControlBar.setVisibility(View.INVISIBLE);
        }
        isShowing = false;
    }

    private final Runnable mFadeOut = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return !isShowing;
    }

    private class VideoDurationChangedHandler implements EventListener {
        private VideoDurationChangedHandler() {
        }

        @Default
        public void processEvent(Event event) {
            int duration = event.getIntegerProperty("duration");
            mEndTime.setText(StringUtil.stringForTime((long) duration));
            endTime = duration;
        }
    }

    private class ProgressHandler implements EventListener {
        private ProgressHandler() {
        }

        @Default
        public void processEvent(Event event) {
//            if(!isDragging()) {
                int position = event.getIntegerProperty("playheadPosition");
                if(mCurrTime != null) {
                    mCurrTime.setText(StringUtil.stringForTime((long)position));
                }

                int duration = event.getIntegerProperty("duration");
                if(!mPlayer.getVideoDisplay().isLive() && mEndTime != null) {
                    mEndTime.setText(StringUtil.stringForTime((long)duration));
                }
                mSeekBar.setProgress((int) ((float) (position * 100/ duration)));
//            } else {
//                Log.d(TAG, "The seek bar is being dragged.  No progress updates are being applied.");
//            }

        }
    }

    private class SeekToHandler implements EventListener {
        private SeekToHandler() {
        }

        @Default
        public void processEvent(Event event) {
//            if(!.this.isDragging()) {
                int position;
                if(event.properties.containsKey("originalSeekPosition")) {
                    position = event.getIntegerProperty("originalSeekPosition");
                } else {
                    position = event.getIntegerProperty("seekPosition");
                }

                if(mCurrTime != null) {
                    mCurrTime.setText(StringUtil.stringForTime((long)position));
                }
//            } else {
//                Log.d(TAG, "The seek bar is being dragged.  No SEEK_TO updates are being applied.");
//            }

        }
    }

    /** Double tap testing **/
    private final GestureDetector doubleTapDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.v(TAG, "DoubleTap");
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);

        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    });


}
